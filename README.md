<div align="center">

<img src="./images/logo.svg" width="256" />

<br />

# ARASAKA.DOT

These are my dot files themed around Arasaka from the game [Cyberpunk 2077](https://www.cyberpunk.net/us/en/).

[Screenshots](https://gitlab.com/pSchwietzer/arasaka-dot/-/tree/main/images/screenshots)

<br />

<img src="./images/screenshots/001.png" width="768" />

</div>

<br /><br />

# Configuration

For conversions of numerals to japanese numerals I use my crate [Suukon](https://crates.io/crates/suukon)

<table>
    <tr>
        <th>Category</th>
        <th>Used</th>
    </tr>
    <tr>
        <td>Window Manager</td>
        <td>[HYPRLAND](https://github.com/hyprwm/Hyprland)</td>
    </tr>
    <tr>
        <td>Bar</td>
        <td>[WAYBAR](https://github.com/Alexays/Waybar)</td>
    </tr>
    <tr>
        <td>Terminal</td>
        <td>[ALACRITTY](https://github.com/alacritty/alacritty)</td>
    </tr>
    <tr>
        <td>Editor</td>
        <td>[NEOVIM](https://github.com/neovim/neovim), [HELIX](https://github.com/helix-editor/helix)</td>
    </tr>
    <tr>
        <td>Fetch</td>
        <td>[FETCH.NU](scripts/fetch.nu)</td>
    </tr>
    <tr>
        <td>Menu</td>
        <td>[WOFI](https://hg.sr.ht/~scoopta/wofi)</td>
    </tr>
    <tr>
        <td>Notification</td>
        <td>[DUNST](https://github.com/dunst-project/dunst)</td>
    </tr>
    <tr>
        <td>Screenshot</td>
        <td>[GRIM](https://sr.ht/~emersion/grim/)</td>
    </tr>
    <tr>
        <td>Input</td>
        <td>[FCITX5](https://github.com/fcitx/fcitx5)</td>
    </tr>
    <tr>
        <td>Terminal Muxer</td>
        <td>[ZELLIJ](https://github.com/zellij-org/zellij)</td>
    </tr>
</table>

## Disclaimer

This repository includes logos and copyrighted material from [Cyberpunk 2077](https://www.cyberpunk.net/us/en/). These assets are used solely for personal and non-commercial purposes to customize the desktop environment and theming this repository. All copyrights for the original assets belong to their respective owners.
