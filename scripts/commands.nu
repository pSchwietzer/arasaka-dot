def toggle_audio_device [] {
  let sink_1 = "alsa_output.usb-Logitech_G432_Gaming_Headset_000000000000-00.analog-stereo"
  let sink_2 = "alsa_output.pci-0000_0a_00.1.pro-output-9"
  let sink_3 = "alsa_output.usb-Yamaha_Corporation_Yamaha_AG06MK2-00.analog-stereo"

  let current_sink = (pactl info | grep "デフォルトシンク" | awk '{print $2}')

  if $current_sink == $sink_1 {
      pactl set-default-sink "$sink_2"
      echo "Switched to $sink_2"
  } else if $current_sink == $sink_2 {
      pactl set-default-sink "$sink_3"
      echo "Switched to $sink_3"
  } else {
      pactl set-default-sink "$sink_1"
      echo "Switched to $sink_1"
  }
}

def main [x: string] {
  match $x {
    "audio-device" => toggle_audio_device,
  }
}
