source ~/.config/nushell/env.nu

def audio_device [] {
  while true {
    let sink_1 = "alsa_output.usb-Logitech_G432_Gaming_Headset_000000000000-00.analog-stereo"
    let sink_2 = "alsa_output.pci-0000_0a_00.1.pro-output-9"
    let sink_3 = "alsa_output.usb-Yamaha_Corporation_Yamaha_AG06MK2-00.analog-stereo"

    let current_sink = pactl info | grep "デフォルトシンク" | awk '{print $2}'

    mut device = ""
    if $current_sink == $sink_1 {
      $device = "ヘッドセット"
    } else if $current_sink == $sink_2 {
      $device = "ディスプレイスピーカー"
    } else if $current_sink == $sink_3 {
      $device = "ミキサー"
    } else {
      $device = "不明"
    }

    print $device
    sleep 1sec
  }
}

def cpu_tmp [] {
  while true {
    let temp = sys | get temp | find "asusec CPU" | get temp | get 0 | into int
    let temp = suukon -t japanese -s financial digit_only -- $temp

    print ($temp + "度")
    sleep 1sec
  }
}

def cpu_load [] {
  while true {
    let core_loads = sys | get cpu | get cpu_usage

    mut total_load = 0
    for core_load in $core_loads {
      $total_load = $total_load + $core_load
    }

    mut avg_load = 0
    $avg_load = $total_load / ($core_loads | length)

    mut avg_load = $avg_load | into int
    if $avg_load > 99 {
      $avg_load = 99
    }
    
    let converted = suukon -t japanese -s financial digit_only -- $avg_load
    mut load = $converted

    let length = $converted | str length --grapheme-clusters
    if $length == 1 {
      $load = "　" + $converted
    }

    print ($load + "率")
    sleep 1sec
  }
}

def network_name [] {
  while true {
    let data = nmcli c

    if ($data | str contains " vpn ") {
      print "仮想閉域網"
    } else if ($data | str contains " ethernet ") {
      print "イーサネット"
    } else {
      print "無接続"
    }

    sleep 1sec
  }
}

def get_net_speed [down: bool] {
  let interface = ip addr | awk '/state UP/ {print $2}' | sed 's/.$//'
  let bytes = cat /proc/net/dev | grep $interface | cut -d ':' -f 2 | awk '{print "DOWN_BYTES="$1, "UP_BYTES="$9}' | split row ' ' | split column '='

  mut old_down_bytes = ($bytes.column2.0 | into int)
  mut old_up_bytes = ($bytes.column2.1 | into int)

  while true {
    let bytes = cat /proc/net/dev | grep $interface | cut -d ':' -f 2 | awk '{print "DOWN_BYTES="$1, "UP_BYTES="$9}' | split row ' ' | split column '='

    mut new_bytes = 0
    if $down {
      $new_bytes = ($bytes.column2.0 | into int)
    } else {
      $new_bytes = ($bytes.column2.1 | into int)
    }

    mut old_bytes = 0
    if $down {
      $old_bytes = ($old_down_bytes | into int)
    } else {
      $old_bytes = ($old_up_bytes | into int)
    }

    let velocity = $new_bytes - $old_bytes
    let velocity_kb = $velocity / 1024
    let velocity_mb = $velocity_kb / 1024

    mut velocity = ""
    if $velocity_mb == 0 {
      $velocity = "0.00"
    } else {
      $velocity = ($velocity_mb | into string)
    }

    mut velocity = $velocity | into string | split row '.'
    if ($velocity | length) == 1 {
      $velocity = $velocity 
    }

    mut integer = $velocity | get 0
    if $integer == "" {
      $integer = "0"
    }

    mut decimal = $velocity | get 1 | str substring --grapheme-clusters 0..2
    if $decimal == "" {
      $decimal = "00"
    }

    let velocity = suukon -t japanese -s financial digit_only -- ($integer + "." + $decimal)

    print ($velocity + "メガ秒速")

    if $down {
      $old_down_bytes = $bytes.column2.0
    } else {
      $old_up_bytes = $bytes.column2.1
    }

    sleep 1sec
  }
}

def get_time [] {
  while true {
    let date = date now
    let weekday = ($date | format date "%A")
    let date = $date | date to-record

    let day = suukon -t japanese -s financial digit_only -- $date.day
    let month = suukon -t japanese -s financial digit_only -- $date.month
    let year = suukon -t japanese -s financial digit_only -- $date.year

    mut hour = $date.hour
    if $date.hour < 10 {
      $hour = "0" + ($date.hour | into string)
    }

    mut minute = $date.minute
    if $date.minute < 10 {
      $minute = "0" + ($date.minute | into string)
    }

    mut second = $date.second
    if $date.second < 10 {
      $second = "0" + ($date.second | into string)
    }

    let hour = suukon -t japanese -s financial digit_only -- $hour
    let minute = suukon -t japanese -s financial digit_only -- $minute
    let second = suukon -t japanese -s financial digit_only -- $second

    print ($year + "年 " + $month + "月 " + $day + "日　" + $weekday + "　" + $hour + "：" + $minute + "：" + $second)
    sleep 1sec
  }
}

def get_uptime [] {
  while true {
    let total_seconds = sys | get host.uptime | format duration sec | sed -e 's/ sec//g' | into int

    mut minutes = ($total_seconds / 60 | into int) mod 60
    mut hours = ($total_seconds / 3600 | into int) mod 24
    mut days = $total_seconds / 86400 | into int

    let minutes = suukon -t japanese -s financial digit_only -- $minutes
    let hours = suukon -t japanese -s financial digit_only -- $hours
    let days = suukon -t japanese -s financial digit_only -- $days

    print ($days + "日 " + $hours + "時 " + $minutes + "分")
    sleep 1sec
  }
}

def get_volume [] {
  while true {
    let current_sink = pactl get-default-sink

    let mute = pactl get-sink-mute $current_sink | grep Mute | head -n1 | awk '{print $2}'
    mut is_mute = false
    if $mute == "はい" {
      $is_mute = true
    }

    if $is_mute {
      print "ミュート"
    } else {
      let volume = pactl get-sink-volume $current_sink | grep Volume | head -n3 | awk '{print $5}' | str trim --char '%' | into int
      let volume = suukon -t japanese -s financial digit_only -- $volume
      print ($volume + "率")
    }

    sleep 100ms
  }
}

def get_memory [] {
  while true {
    let mem = sys | get mem
    let total = $mem | get total | into filesize | format filesize gib | sed 's/\ GiB//'
    let used = $mem | get used | into filesize | format filesize gib | sed 's/\ GiB//'
    
    let total = suukon -t japanese -s financial digit_only -- $total
    let used = suukon -t japanese -s financial digit_only -- $used

    print ($used + "ギガ｜" + $total + "ギガ")
    sleep 1sec
  }
}

def get_swap [] {
  while true {
    let swap = sys | get mem
    let total = $swap | get "swap total" | into filesize | format filesize gib | sed 's/\ GiB//'
    let used = $swap | get "swap used" | into filesize | format filesize gib | sed 's/\ GiB//'
    
    let total = suukon -t japanese -s financial digit_only -- $total
    let used = suukon -t japanese -s financial digit_only -- $used

    print ($used + "ギガ｜" + $total + "ギガ")
    sleep 1sec
  }
}

def get_kernel [] {
  let kernel = sys | get host.kernel_version | split row '-' | get 0 | split row '.'

  mut converted = ""
  mut counter = 0
  for num in $kernel {
    let tmp = suukon -t japanese -s financial digit_only -- $num

    if $counter == 0 {
      $converted = $tmp
    } else {
      $converted = $converted + "点" + $tmp
    }

    $counter = $counter + 1
  }

  print $converted
}

def main [x: string] {
  match $x {
    "cpu-load" => cpu_load,
    "cpu-temp" => cpu_tmp,
    "net-down" => { get_net_speed true },
    "net-up" => { get_net_speed false },
    "net-name" => network_name,
    "volume" => get_volume,
    "time" => get_time,
    "uptime" => get_uptime,
    "memory" => get_memory,
    "swap" => get_swap,
    "kernel" => get_kernel,
    "audio-device" => audio_device,
  }
}
