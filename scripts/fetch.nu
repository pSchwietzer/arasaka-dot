let sys = sys
let mem = cat /proc/meminfo | split row "\n" | split column -c ':'
let logo = "
                 ......                 
              .cx0XXXX0xc.              
             ;0WMMMMMMMMW0;             
            '0MMMMMMMMMMMM0'            
      ..    ;XMMMMMMMMMMMMX;    ..      
  .cxO00Oxc.'kWMMMMMMMMMMWk'.cxO00Oxc.  
.lXMMMMMMMWKl;dXWMMMMMMWXd;lKWMMMMMMWXl.
lNMMMMMMMMMMNl..cOWMMWOc..lNMMMMMMMMMMNl
kMMMMMMMMMMMMx.  cNMMNc  .xMMMMMMMMMMMMk
lNMMMMMMMMMMNl   cNMMNc   lNMMMMMMMMMMNl
.cKWMMMMMMMMNx'  cNMMNc  'xNMMMMMMMMWKc.
  .cdk00kdxKWMXo'lNMMNl'oXMWKxdkO0kdc.  
           .oKWMXXWMMWXXWWKo.           
             .oKWMMMMMMWKo.             
               .oXMMMMXo.               
                 lNMMNl                 
                 cNMMNc                 
                 cNMMNc                 
                 .lool.                 
" | split row "\n"

def get_uptime [] {
  let total_seconds = cat /proc/uptime | split row '.' | get 0 | into int

  mut minutes = ($total_seconds / 60 | into int) mod 60
  mut hours = ($total_seconds / 3600 | into int) mod 24
  mut days = $total_seconds / 86400 | into int

  let minutes = suukon -t japanese -s financial digit_only -- $minutes
  let hours = suukon -t japanese -s financial digit_only -- $hours
  let days = suukon -t japanese -s financial digit_only -- $days

  return ($days + "日 " + $hours + "時 " + $minutes + "分")
}

def get_memory [] {
  let total = $mem | find MemTotal | get column2.0 | str trim | into filesize
  let free = $mem | find MemAvailable | get column2.0 | str trim | into filesize

  let used = ($total - $free | format filesize gib | sed 's/\ GiB//')
  let total = $total | format filesize gib | sed 's/\ GiB//'
  
  let total = suukon -t japanese -s financial digit_only -- $total
  let used = suukon -t japanese -s financial digit_only -- $used

  return ($used + "ギガ｜" + $total + "ギガ")
}

def get_swap [] {
  let total = $mem | find SwapTotal | get column2.0 | str trim | into filesize
  let free = $mem | find SwapFree | get column2.0 | str trim | into filesize

  let used = ($total - $free | format filesize gib | sed 's/\ GiB//')
  let total = $total | format filesize gib | sed 's/\ GiB//'
  
  let total = suukon -t japanese -s financial digit_only -- $total
  let used = suukon -t japanese -s financial digit_only -- $used

  return ($used + "ギガ｜" + $total + "ギガ")
}

def get_distro [] {
  let $version = suukon -t japanese -s financial digit_only -- (cat /etc/os-release | split row "\n" | find VERSION_ID | get 0 | str substring 11..13)
  let distro = ("フェドラ" + $version)

  return $distro
}

def get_kernel [] {
  let kernel = uname -r | split row '-' | get 0 | split row '.'

  mut converted = ""
  mut counter = 0
  for num in $kernel {
    let tmp = suukon -t japanese -s financial digit_only -- $num

    if $counter == 0 {
      $converted = $tmp
    } else {
      $converted = $converted + "点" + $tmp
    }

    $counter = $counter + 1
  }

  return $converted
}

def get_disk [device: string, metric: string] {
  let disks = ($sys | get disks)
  mut disk = $disks.0
  for d in $disks {
    if $d.device == $device {
      $disk = $d
    }
  }

  let free = $disk.free
  let total = $disk.total

  let used = match $metric {
    "tb" => ($total - $free | format filesize tb | sed 's/\ TB//'),
    "gb" => ($total - $free | format filesize gb | sed 's/\ GB//'),
  }
  let total = match $metric {
    "tb" => ($total | format filesize tb | sed 's/\ TB//'),
    "gb" => ($total | format filesize gb | sed 's/\ GB//'),
  }

  let used = suukon -t japanese -s financial digit_only -- $used
  let total = suukon -t japanese -s financial digit_only -- $total
  let metric = match $metric {
    "tb" => "テラ",
    "gb" => "ギガ",
  }

  return ($used + $metric + "｜" + $total + $metric)
}

def get_cpu [] {
  return "AMD RYZEN 7 3700X"
}

def get_gpu [] {
  return "AMD RADEON RX 6600"
}

def main [] {
  let cols = term size | get columns

  let distro = get_distro
  let $uptime = get_uptime
  let $kernel = get_kernel
  let $cpu = get_cpu
  let $gpu = get_gpu
  let $memory = get_memory
  let $swap = get_swap

  let root = get_disk /dev/nvme0n1p3 gb
  let home = get_disk /dev/nvme0n1p5 gb
  let wd_ultra = get_disk /dev/sda tb
  let toshiba_hdd = get_disk /dev/sdb1 tb

  let distro = $"(ansi red)システム　(ansi white)($distro)(ansi reset)"
  let kernel = $"(ansi red)カーネル　(ansi white)($kernel)(ansi reset)"
  let uptime = $"(ansi red)稼働時間　(ansi white)($uptime)(ansi reset)"
  let cpu = $"(ansi red)中央装置　(ansi white)($cpu)(ansi reset)"
  let gpu = $"(ansi red)画像装置　(ansi white)($gpu)(ansi reset)"
  let mem = $"(ansi red)記憶装置　(ansi white)($memory)(ansi reset)"
  let swap = $"(ansi red)スワップ　(ansi white)($swap)(ansi reset)"
  let root = $"(ansi red)　ルート　(ansi white)($root)(ansi reset)"
  let home = $"(ansi red)　ホーム　(ansi white)($home)(ansi reset)"
  let wd_ultra = $"(ansi red)ハード壱　(ansi white)($wd_ultra)(ansi reset)"
  let toshiba_hdd = $"(ansi red)ハード弐　(ansi white)($toshiba_hdd)(ansi reset)"

  let info = [
    "",
    "",
    $distro,
    $kernel,
    $uptime,
    "",
    $cpu,
    $gpu,
    $mem,
    $swap,
    "",
    $root,
    $home,
    $wd_ultra,
    $toshiba_hdd,
    "",
    "",
    "",
    "",
    "",
  ]

  let left_padding = "  "
  let mid_padding = "　　　　"

  let rows = $logo | length
  for row in 0..($rows - 1) {
    let line = $row
    let logo_line = ($logo | get $line)
    if $logo_line == "" {
      continue
    }

    let info_line = ($info | get $line)
    
    #if $row mod 5 == 0 {
    #  sleep 1ns
    #}

    let line = ($left_padding + ($logo_line) + $mid_padding + $info_line)

    mut end_col = 0
    if ($line | str length --grapheme-clusters) > $cols {
      $end_col = $cols
    } else {
      $end_col = ($line | str length --grapheme-clusters)
    }

    print ($"(ansi reset)" + $line | str substring --grapheme-clusters 0..($end_col))
  }
}
