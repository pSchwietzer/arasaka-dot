def main [] {
  let files = ls **/*.nfo | get name

  let length = $files | length
  echo ("Found " + ($length | into string) + " files")

  echo ("Starting processing")

  for file in $files {
    echo ("Processing: " + $file)

    full2half -j -k -f $file
    #full2half -r -k -a -s -f $file

    echo ("Finished processing: " + $file)
  }

  echo ("Finished processing")
}
