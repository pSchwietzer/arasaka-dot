def main [] {
  let episodes = ls **/*.nfo | get name
  let length = $episodes | length

  mut list = []

  for episode in $episodes {
    if ($episode | str contains "season.nfo") or ($episode | str contains "tvshow.nfo") {
      continue
    }

    echo ("CHECKING EPISODE: " + $episode)

    let xml = cat $episode | from xml | get content
    let title =  ($xml | find "title" | get content | get 0 | get content.0)
    #echo ("JELLYFIN TITLE:  " + $title)

    let name = $episode | str replace ".nfo" ""
    let mkv = ($name + ".mkv")
    let video_title = mkvinfo $mkv | grep "タイトル:" | sed 's/^.*: //' | awk '{$1=$1};1'
    #echo ("VIDEO TITLE:     " + $video_title)

    if $title != $video_title {
      $list = ($list | append {nfo: $episode, title: $title, video_title: $video_title})
    }

  }

  if ($list | length) == 0 {
    echo "\nALL TITLES MATCH"
    exit 0
  } else {
    echo "\nSOME TITLES DON'T MATCH\n"

    for item in $list {
      echo ("WRONG TITLE: " + $item.nfo)
      echo ($item.title + "\n" + $item.video_title)
      echo ""
    }
  }
}
