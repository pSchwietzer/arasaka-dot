source ~/.bash_env.sh

if [ -z "$1"]
then
  cd ~/ドキュメント/開発/
  zellij -l neovim
else
  cd "$(dirname "$1")"
  nvim "$1"
  #zellij attach -c $(basename "$1")
  #zellij action edit "$1"
  #zellij kill-session $(basename "$1")
  sleep 10
fi
