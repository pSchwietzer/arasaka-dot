source ~/.bash_env.sh

VALID_ARGS=$(getopt -o udwsia --long up,down,wofi,workspace,idle,audio-device -- "$@")
if [[ $? -ne 0 ]]
then
    exit 1;
fi

eval set -- "$VALID_ARGS"
while [ : ];
do
  case "$1" in
    -u | --up) METRIC="UP" shift ;;
    -d | --down) METRIC="DOWN" shift ;;
    -w | --wofi) METRIC="WOFI" shift ;;
    -s | --workspace) METRIC="WORKSPACES"; WORKSPACE=$3; shift ;;
    -i | --idle) METRIC="IDLE" shift ;;
    -a | --audio-device) METRIC="AUDIO-DEVICE" shift ;;
    --) shift; break;;
  esac
done

if [[ $METRIC = "AUDIO-DEVICE" ]]
then
  SINK1="alsa_output.usb-Logitech_G432_Gaming_Headset_000000000000-00.analog-stereo"
  SINK2="alsa_output.pci-0000_0a_00.1.pro-output-9"
  SINK3="alsa_output.pci-0000_0c_00.4.analog-stereo"

  CURRENT_SINK=$(pactl info | grep "デフォルトシンク" | awk '{print $2}')

  if [ "$CURRENT_SINK" == "$SINK1" ]; then
      pactl set-default-sink "$SINK2"
      echo "Switched to $SINK2"
  elif [ "$CURRENT_SINK" == "$SINK2" ]; then
      pactl set-default-sink "$SINK3"
      echo "Switched to $SINK3"
  else
      pactl set-default-sink "$SINK1"
      echo "Switched to $SINK1"
  fi
fi

if [[ $METRIC = "IDLE" ]]
then
#swayidle \
#    timeout 1 'swaymsg "output * dpms off"' \
#    resume 'swaymsg "output * dpms on"' &
#swaylock -c000000
#kill %%

  # Check if the screen is on or off
  screen_status=$(swaymsg -t get_outputs | jq -r '.[] | select(.active) | .dpms')
  echo "$screen_status"

  if [[ $screen_status == *true* ]]; then
      screen_status=true
  else
      screen_status=false
  fi

  echo "$screen_status"

  if [[ "$screen_status" == "true" ]]; then
      swaymsg "output * dpms off"
  else
      swaymsg "output * dpms on"
  fi
fi

if [[ $METRIC = "WOFI" ]]
then
  DATA=$(swaymsg -t get_outputs | grep 'focused')
  HDMI=$(echo "$DATA" | sed '1d' | cut -d':' -f 2 | sed 's/ //g' | sed 's/,//g')
  DPI=$(echo "$DATA" | sed '2d' | cut -d':' -f 2 | sed 's/ //g' | sed 's/,//g')
  echo "$DPI"
  echo "$HDMI"
  # Script for switching workspace on sway
  # flag passes workspace number
  # switch to workspace, switch to workspace flag + 10 refocus on old workspace
fi

if [[ $METRIC = "WORKSPACES" ]]
then
  DATA=$(swaymsg -r -t get_outputs | grep 'name')
  INPUT=$(swaymsg -r -t get_outputs | jq '.[] | select(.focused==true) | .name' | sed 's/"//g')
  INPUT_1=$(echo "$DATA" | sed '2d' | cut -d':' -f 2 | sed 's/ //g' | sed 's/,//g' | sed 's/"//g')
  INPUT_2=$(echo "$DATA" | sed '1d' | cut -d':' -f 2 | sed 's/ //g' | sed 's/,//g' | sed 's/"//g')

  FOCUSED_DATA=$(swaymsg -t get_outputs | grep 'focused')
  INPUT_1_FOCUS=$(echo "$FOCUSED_DATA" | sed '2d' | cut -d':' -f 2 | sed 's/ //g' | sed 's/,//g')
  INPUT_2_FOCUS=$(echo "$FOCUSED_DATA" | sed '1d' | cut -d':' -f 2 | sed 's/ //g' | sed 's/,//g');
  
  if [[ $WORKSPACE = "next" ]]
  then
    WORKSPACE=$(swaymsg -r -t get_workspaces | jq '.[] | select(.focused==true) | .num')

    if [[ ${WORKSPACE:1} = "0" ]]
    then
      WORKSPACE="${WORKSPACE:1}"
    elif [[ ${WORKSPACE:1} != "" ]]
    then
      WORKSPACE="${WORKSPACE:1}"
    fi

    if [[ $WORKSPACE = "10" ]]
    then
      WORKSPACE="1"
    else
      WORKSPACE=$(($WORKSPACE + 1))
    fi
  fi
  
  if [[ $WORKSPACE = "prev" ]]
  then
    WORKSPACE=$(swaymsg -r -t get_workspaces | jq '.[] | select(.focused==true) | .num')

    if [[ ${WORKSPACE:1} = "0" ]]
    then
      WORKSPACE="10"
    elif [[ ${WORKSPACE:1} != "" ]]
    then
      WORKSPACE="${WORKSPACE:1}"
    fi

    if [[ $WORKSPACE = "1" ]]
    then
      WORKSPACE="10"
    else
      WORKSPACE=$(($WORKSPACE - 1))
    fi
  fi

  WORKSPACE_SYNCED=$(($WORKSPACE + 10))
  swaymsg workspace "$WORKSPACE_SYNCED"
  swaymsg workspace "$WORKSPACE"

  if [[ $INPUT_1_FOCUS = "true" ]]
  then
    swaymsg focus output "$INPUT_1"
  elif [[ $INPUT_2_FOCUS = "true" ]]
  then
    swaymsg focus output "$INPUT_2"
  fi
fi

ACTIVE_SINK=$(pactl get-default-sink)
VOLUME=$(pactl get-sink-volume "$ACTIVE_SINK" | grep Volume | head -n3 | awk '{print $5}')
VOLUME="${VOLUME::-1}"

UP=$(("$VOLUME" % "5"))
if [[ "$UP" = "0" ]]
then
  UP="5"  
else
  UP=$(("5" - "$UP"))
fi

DOWN=$(("$VOLUME" % "5"))
if [[ "$DOWN" = "0" ]]
then
  DOWN="5"  
fi

if [[ $VOLUME = "200" ]]
then
  UP="0"
fi

if [[ $METRIC = "UP" ]]
then
  pactl set-sink-volume @DEFAULT_SINK@ "+${UP}%"
elif [[ $METRIC = "DOWN" ]]
then  
  pactl set-sink-volume @DEFAULT_SINK@ "-${DOWN}%"
fi

ACTIVE_SINK=$(pactl get-default-sink)
DATA=$(pactl get-sink-volume "$ACTIVE_SINK" | grep Volume | head -n3 | awk '{print $5}')
MUTE=$(pactl get-sink-mute "$ACTIVE_SINK" | grep Mute | head -n1 | awk '{print $2}')
VOLUME="${DATA::-1}"

if [[ ${VOLUME::-1} = "" ]]
then
  FIRST="0"
  SECOND="${VOLUME}"
elif [[ ${VOLUME::-2} != "" ]]
then
  FIRST="${VOLUME::-1}"
  SECOND="${VOLUME:2}"
else
  FIRST="${VOLUME::-1}"
  SECOND="${VOLUME:1}"
fi
  
if [[ ${FIRST::-1} != ""  ]]
then
  ZERO="${FIRST::-1}"
  FIRST="${FIRST:1}"
else
  ZERO="0"
fi

if [[ $MUTE = "はい" ]]
then
  VOLUME="零点零分零厘"   
elif [[ $FIRST = "0" ]]
then
  if [[ $SECOND = "0" ]]
  then
    if [[ $ZERO != "0" ]]
    then
      VOLUME="$(suukon "${ZERO}")点$(suukon "${FIRST}")分$(suukon "${SECOND}")厘"
    else
      VOLUME="零点零分零厘"
    fi
  else
    VOLUME="$(suukon "${ZERO}")点$(suukon "${FIRST}")分$(suukon "${SECOND}")厘"
  fi  
else
  VOLUME="$(suukon "${ZERO}")点$(suukon "${FIRST}")分$(suukon "${SECOND}")厘"
fi
