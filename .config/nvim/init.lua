------------------------------------------------------------------
--                           基本設定                           --
------------------------------------------------------------------
-- NEOVIMの基本設定。                                           --
-- https://neovim.io/doc/user/options.html                      --
-- https://neovim.io/doc/user/quickref.html#option-list         --
------------------------------------------------------------------

  local set = vim.opt

  set.number = true -- ラインナンバーを表示
  set.cursorline = true -- カーソルラインをハイライトする
  set.tabstop = 2 -- <Tab>のスペース数
  set.shiftwidth = 2 -- 自動インデントのスペース数
  set.softtabstop = 0 -- <Tab>のスペース数
  set.expandtab = true -- タブ文字よりに空白文字を使う
  set.swapfile = false
  set.undolevels = 100000 -- 元に戻すの出来る数
  set.undofile = true -- 元に戻す履歴をファイルに保存する
  set.termguicolors = true -- 24-BITカラーを有効
  set.wrap = true -- 折り返しを有効する
  set.fillchars = 'eob: ,vert:│,diff:╱' -- 充填文字を設置
  set.showtabline = 0 -- タッブラインを隠す
  set.showmode = false -- コマンドラインにモードを隠す
  set.showcmd = false -- コマンドラインに選択数を隠す
  set.cursorcolumn = false
  set.signcolumn = 'yes:1'
  set.guicursor = 'a:block-blinkon400' -- カーソル形を設置
  set.clipboard = 'unnamedplus' -- システムクリップボードを使用
  vim.o.shell = "/bin/nu" -- シェルを設置
  set.foldmethod = "indent"
  set.foldlevel = 99
  vim.opt.mouse = ""
------------------------------------------------------------------
--                          プラグイン                          --
------------------------------------------------------------------
-- インストールしたプラグインを有効する。                       --
------------------------------------------------------------------

  local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
  if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
      "git",
      "clone",
      "--filter=blob:none",
      "https://github.com/folke/lazy.nvim.git",
      "--branch=stable", -- latest stable release
      lazypath,
    })
  end
  vim.opt.rtp:prepend(lazypath)

  require('lazy').setup('plugins', {
    change_detection = {
      notify = false,
    }
  })

  require('arasaka')
  require('filetree')
  require('source-control')
  require('statusline')

  require("indent_blankline").setup {
    show_current_context = true,
    show_current_context_start = false,
    space_char_blankline = " "
  }
  require("nvim-highlight-colors").setup {
    render = 'background',
    enable_named_colors = true,
    enable_tailwind = true
  }

  require('lsp')
  require('complete')

  require('plugins')
  require('keymap')

------------------------------------------------------------------
--                        初期のコマンド                        --
------------------------------------------------------------------
-- 初期の実施するコマンド。                                     --
------------------------------------------------------------------

  -- カラーテーマを設置
  vim.api.nvim_command('colorscheme arasaka')

  -- ファイルを開いた時の挙動を設置
  if next(vim.fn.argv()) == nil then
    vim.api.nvim_command('cd ~/ドキュメント/開発/')
  else
    local first_arg = vim.fn.argv()[1]
    local is_directory = vim.fn.isdirectory(first_arg) == 1

    if is_directory then
      vim.cmd('cd ' .. first_arg)
    else
      vim.cmd('cd ' .. vim.fn.fnamemodify(first_arg, ':h'))
    end
  end
