------------------------------------------------------------------
--                       カラーパレット                         --
------------------------------------------------------------------
-- ARASAKAカラーテーマのカラーパレット。                        --
------------------------------------------------------------------

  return {
    red = '#ff2250',
    cyan = '#00eaff',
    green = '#00ffaa',
    yellow = '#ffdd00',
  	orange = '#ff8829',
    purple = '#a84afe',
    pink = '#ff00ff',
  	brown = '#504945',
    black = '#000000',
    base1 = '#101010',
    base2 = '#202020',
    base3 = '#303030',
    base4 = '#404040',
    base5 = '#505050',
    base6 = '#606060',
    base7 = '#707070',
    base8 = '#808080',
    base9 = '#909090',
    baseA = '#AAAAAA',
    baseB = '#BBBBBB',
    baseC = '#CCCCCC',
    baseD = '#DDDDDD',
    baseE = '#EEEEEE',
    white = '#F0F0F0',
  	diff_add = '#004422',
    diff_delete = '#4a0f23',
    diff_change = '#151515',
    diff_text = '#23324d',
    outline = '#353535',
  }
