local palette = require('palette')

------------------------------------------------------------------
--                          モード翻訳                          --
------------------------------------------------------------------
-- モードを日本語に翻訳する。                                   --
------------------------------------------------------------------

  local modes = {
    ["n"] = "ノーマル",
    ["no"] = "ノーマル",
    ["v"] = "ビジュアル",
    ["V"] = "ビジュアル・ライン",
    [""] = "ビジュアル・ブロック",
    ["s"] = "選択",
    ["S"] = "選択・ライン",
    [""] = "選択・ブロック",
    ["i"] = "挿入",
    ["ic"] = "挿入",
    ["R"] = "置換",
    ["Rv"] = "ビジュアル・置換",
    ["c"] = "コマンド",
    ["cv"] = "コマンド",
    ["ce"] = "コマンド",
    ["r"] = "プロンプト",
    ["rm"] = "コマンド",
    ["r?"] = "確認",
    ["!"] = "シェル",
    ["t"] = "ターミナル",
  }

  local function mode()
    local current_mode = vim.api.nvim_get_mode().mode
    local mode = string.format(" %s ", modes[current_mode]):upper()
    return mode
  end

------------------------------------------------------------------
--                        LSP-PROGRESS                          --
------------------------------------------------------------------
-- https://github.com/arkav/lualine-lsp-progress                --
------------------------------------------------------------------

  local lsp_client = {
  	'lsp_progress',
  	display_components = { 'lsp_client_name', 'spinner' },
  	colors = {
  	  spinner = palette.white,
  	  lsp_client_name = palette.red,
  	  use = true
  	},
  	separators = {
  		component = ' ',
  		lsp_client_name = { pre = '', post = '' },
  		spinner = { pre = '', post = '' }
  	},
  	timer = { progress_enddelay = 500, spinner = 1000, lsp_client_name_enddelay = 1000 },
  	spinner_symbols = { '🌑 ', '🌒 ', '🌓 ', '🌔 ', '🌕 ', '🌖 ', '🌗 ', '🌘 ' }
  }

------------------------------------------------------------------
--                           LUALINE                            --
------------------------------------------------------------------
-- https://github.com/nvim-lualine/lualine.nvim                 --
------------------------------------------------------------------

  local theme = {
    normal = {
      a = { fg = palette.red, bg = palette.black },
      b = { fg = palette.white, bg = palette.black },
      c = { fg = palette.white, bg = palette.black },
      x = { fg = palette.white, bg = palette.black },
      y = { fg = palette.white, bg = palette.black },
      z = { fg = palette.white, bg = palette.black },
    },
    --insert = { a = { fg = palette.green, bg = palette.black } },
    --visual = { a = { fg = palette.cyan, bg = palette.black } },
    --replace = { a = { fg = palette.pink, bg = palette.black } },
    --terminal = { a = { fg = palette.yellow, bg = palette.black } },
  }

  require('lualine').setup {
    options = {
      icons_enabled = true,
      theme = theme,
      globalstatus = true,
      component_separators = { left = '', right = ''},
      section_separators = { left = '', right = ''},
      -- disabled_filetypes = {
      --   statusline = {'NvimTree', 'toggleterm'},
      --   winbar = {'NvimTree', 'toggleterm'},
      -- },
      refresh = {
        statusline = 1000,
        tabline = 1000,
        winbar = 1000,
      }
    },
    sections = {
      lualine_a = {mode},
      lualine_b = {{'filename', path = 1, file_status = true}, 'diff', 'diagnostics'},
      lualine_c = {lsp_client},
      lualine_x = {'location'},
      lualine_y = {'filesize', 'filetype', 'encoding'},
      lualine_z = {'branch'}
    },
    inactive_sections = {
      lualine_a = {mode},
      lualine_b = {{'filename', path = 1, file_status = true}},
      lualine_c = {},
      lualine_x = {'location'},
      lualine_y = {'filesize', 'filetype', 'encoding'},
      lualine_z = {'branch'}
    },
    tabline = {},
    --winbar = {},
    --inactive_winbar = {},
    extensions = {}
  }
