------------------------------------------------------------------
--                          キーマップ                          --
------------------------------------------------------------------
-- カスタムキーマップを設置。                                   --
------------------------------------------------------------------

  function set_navigation_keymaps()
    cwd = vim.loop.cwd()
    local opts = {buffer = false}
    vim.keymap.set('n', '<C-h>', [[<Cmd>wincmd h<CR>]], opts)
    vim.keymap.set('n', '<C-j>', [[<Cmd>wincmd j<CR>]], opts)
    vim.keymap.set('n', '<C-k>', [[<Cmd>wincmd k<CR>]], opts)
    vim.keymap.set('n', '<C-l>', [[<Cmd>wincmd l<CR>]], opts)
    --vim.keymap.set('n', '<S-h>', [[<Cmd>BufferLineCyclePrev<CR>]], opts)
    --vim.keymap.set('n', '<S-l>', [[<Cmd>BufferLineCycleNext<CR>]], opts)
    --vim.keymap.set('n', '<S-j>', [[<Cmd>BufferLineCyclePrev<CR>]], opts)
    --vim.keymap.set('n', '<S-k>', [[<Cmd>BufferLineCycleNext<CR>]], opts)
    vim.keymap.set('n', 'q', [[<Cmd>Bdelete<CR>]], opts)
    --vim.keymap.set('n', 't', [[<Cmd>execute "!zellij action new-pane -n 'ターミナル' -c --cwd " . getcwd()<CR>]], opts)
    --vim.keymap.set('n', 't', function()
    --vim.keymap.set('n', 'f', [[<Cmd>NvimTreeToggle<CR>]], opts)
  end

  set_navigation_keymaps()

  function _G.set_terminal_keymaps()
    local opts = {buffer = 0}
    vim.keymap.set('t', '<esc>', [[<Cmd>ToggleTerm<CR>]], opts)
    vim.keymap.set('t', 'jk', [[<C-\><C-n>]], opts)
    vim.keymap.set('t', '<S-h>', [[<Cmd>wincmd h<CR>]], opts)
    vim.keymap.set('t', '<S-j>', [[<Cmd>wincmd j<CR>]], opts)
    vim.keymap.set('t', '<S-k>', [[<Cmd>wincmd k<CR>]], opts)
    vim.keymap.set('t', '<S-l>', [[<Cmd>wincmd l<CR>]], opts)
    vim.keymap.set('t', '<S-t>', [[<Cmd>ToggleTerm<CR>]], opts)
  end

  vim.cmd('autocmd! TermOpen term://*toggleterm#* lua set_terminal_keymaps()')

  -- <leader> = '\'
  require('lsp-zero').on_attach(function(client, bufnr)
    local opts = {buffer = bufnr, remap = false}
    vim.keymap.set('n', 'gDd', function() vim.lsp.buf.definition() end, opts)
    vim.keymap.set('n', '<leader>k', function() vim.lsp.buf.hover() end, opts)
    vim.keymap.set('n', '<leader>vws', function() vim.lsp.buf.workspace_symbol() end, opts)
    vim.keymap.set('n', '<leader>vd', function() vim.diagnostic.open_float() end, opts)
    vim.keymap.set('n', '[d', function() vim.diagnostic.goto_next() end, opts)
    vim.keymap.set('n', ']d', function() vim.diagnostic.goto_prev() end, opts)
    vim.keymap.set('n', '<leader>vca', function() vim.lsp.diagnostic.code_action() end, opts)
    vim.keymap.set('n', '<leader>vrr', function() vim.lsp.buf.references() end, opts)
    vim.keymap.set('n', '<leader>vrn', function() vim.lsp.buf.rename() end, opts)
    vim.keymap.set('n', '<leader>lr', [[<Cmd>LspRestart<CR>:echo "Restarting LSP Server"<CR>]], opts)
    --vim.keymap.set('n', '<C-h>', function() vim.lsp.buf.signature_help() end, opts)
  end)

  local telescope = require('telescope.builtin')
  vim.keymap.set('n', '<leader>ff', [[<Cmd>Telescope find_files {hidden=true, disable_devicons=true}<CR>]], {})
  vim.keymap.set('n', '<leader>fg', [[<Cmd>Telescope live_grep {hidden=true, disable_devicons=true}<CR>]], {})
  vim.keymap.set('n', '<leader>ft', [[<Cmd>Telescope file_browser {hidden=true, disable_devicons=true}<CR>]], {})
  vim.keymap.set('n', '<leader>fb', [[<Cmd>Telescope buffers {hidden=true, disable_devicons=true}<CR>]], {})
  vim.keymap.set('n', '<leader>fh', [[<Cmd>Telescope help_tags {hidden=true, disable_devicons=true}<CR>]], {})
  vim.keymap.set('n', '<leader>fd', [[<Cmd>Telescope git_status {hidden=true, disable_devicons=true}<CR>]], {})
