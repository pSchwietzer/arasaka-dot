------------------------------------------------------------------
--                          NVIM-TREE                           --
------------------------------------------------------------------
-- https://github.com/nvim-tree/nvim-tree.lua                   --
------------------------------------------------------------------

--  require('nvim-tree').setup {
--    sort_by = 'case_sensitive',
--    hijack_cursor = true,
--    sync_root_with_cwd = true,
--    view = {
--      side = 'left',
--      adaptive_size = false,
--      width = 40,
--      mappings = {
--        list = {
--          { key = { "<CR>", "o", "<2-LeftMouse>" }, action = "edit" },
--          { key = "<C-e>", action = "" },
--          { key = "O", action = "edit_no_picker" },
--          { key = { "<C-]>", "<2-RightMouse>" }, action = "cd" },
--          { key = "<C-v>", action = "vsplit" },
--          { key = "<C-x>", action = "split" },
--          { key = "<C-t>", action = "" },
--          { key = "<", action = "prev_sibling" },
--          { key = ">", action = "next_sibling" },
--          { key = "P", action = "parent_node" },
--          { key = "<BS>", action = "close_node" },
--          { key = "<Tab>", action = "preview" },
--          { key = "K", action = "first_sibling" },
--          { key = "J", action = "last_sibling" },
--          { key = "C", action = "" },
--          { key = "I", action = "" },
--          { key = "H", action = "" },
--          { key = "B", action = "" },
--          { key = "U", action = "" },
--          { key = "R", action = "refresh" },
--          { key = "a", action = "create" },
--          { key = "d", action = "remove" },
--          { key = "D", action = "trash" },
--          { key = "r", action = "rename" },
--          { key = "<C-r>", action = "full_rename" },
--          { key = "e", action = "" },
--          { key = "x", action = "cut" },
--          { key = "c", action = "copy" },
--          { key = "p", action = "paste" },
--          { key = "y", action = "copy_name" },
--          { key = "Y", action = "copy_path" },
--          { key = "gy", action = "copy_absolute_path" },
--          { key = "[e", action = "" },
--          { key = "[c", action = "" },
--          { key = "]e", action = "" },
--          { key = "]c", action = "" },
--          { key = "-", action = "dir_up" },
--          { key = "s", action = "system_open" },
--          { key = "f", action = "live_filter" },
--          { key = "F", action = "clear_live_filter" },
--          { key = "q", action = "close" },
--          { key = "W", action = "collapse_all" },
--          { key = "E", action = "expand_all" },
--          { key = "S", action = "" },
--          { key = ".", action = "" },
--          { key = "<C-k>", action = "" },
--          { key = "g?", action = "" },
--          { key = "m", action = "" },
--          { key = "bmv", action = "" }
--        },
--      },
--    },
--    renderer = {
--      group_empty = true,
--      root_folder_label = false,
--      indent_width = 2,
--      indent_markers = {
--        enable = true,
--        inline_arrows = true,
--        icons = {
--          corner = '└',
--          edge = '│',
--          item = '│',
--          bottom = '─',
--          none = ' '
--        }
--      },
--      icons = {
--        webdev_colors = false,
--        git_placement = "before",
--        show = {
--          file = true,
--          folder = true,
--          folder_arrow = false,
--          git = true
--        },
--        glyphs = {
--          default = "",
--          symlink = "",
--          bookmark = "",
--          folder = {
--            arrow_closed = "",
--            arrow_open = "",
--            default = "",
--            open = "",
--            empty = "",
--            empty_open = "",
--            symlink = "",
--            symlink_open = "",
--          },
--          git = {
--            unstaged = "✗",
--            staged = "✓",
--            unmerged = "",
--            renamed = "➜",
--            untracked = "★",
--            deleted = "",
--            ignored = "◌",
--          },
--        },
--      },
--      symlink_destination = false,
--    },
--    diagnostics = {
--      enable = false
--    },
--    filters = {
--      dotfiles = false,
--    },
--    actions = {
--      use_system_clipboard = true,
--      change_dir = {
--        enable = true,
--        global = true,
--        restrict_above_cwd = false,
--      }
--    },
--    git = {
--      ignore = false,
--    }
--  }

------------------------------------------------------------------
--                          TELESCOPE                           --
------------------------------------------------------------------
-- https://github.com/nvim-telescope/telescope.nvim             --
------------------------------------------------------------------

local fb_actions = require "telescope._extensions.file_browser.actions"

require('telescope').setup{
  defaults = {
    file_ignore_patterns = { "node_modules", "target", "%.jpg", "%.png", "%.gif" },
  },
  pickers = {
    find_files = {
      hidden = true,
    },
  },
  extensions = {
    file_browser = {
      hijack_netrw = true,
      hidden = true,
      cwd_to_path = true,
      display_stat = { date = false, size = true, mode = false },
      git_status = false,
      grouped = true,
      mappings = {
        ["i"] = {
          ["<A-c>"] = fb_actions.create,
          ["<S-CR>"] = fb_actions.create_from_prompt,
          ["<A-r>"] = fb_actions.rename,
          ["<A-m>"] = fb_actions.move,
          ["<A-y>"] = fb_actions.copy,
          ["<A-d>"] = fb_actions.remove,
          ["<C-o>"] = fb_actions.open,
          ["<C-g>"] = fb_actions.goto_parent_dir,
          ["<C-e>"] = fb_actions.goto_home_dir,
          ["<C-w>"] = fb_actions.goto_cwd,
          ["<C-t>"] = fb_actions.change_cwd,
          [" "] = fb_actions.toggle_browser,
          ["<C-h>"] = fb_actions.toggle_hidden,
          ["<C-s>"] = fb_actions.toggle_all,
          ["<bs>"] = fb_actions.backspace,
        },
        ["n"] = {
          ["c"] = fb_actions.create,
          ["r"] = fb_actions.rename,
          ["m"] = fb_actions.move,
          ["y"] = fb_actions.copy,
          ["d"] = fb_actions.remove,
          ["o"] = fb_actions.open,
          ["g"] = fb_actions.goto_parent_dir,
          ["e"] = fb_actions.goto_home_dir,
          ["w"] = fb_actions.goto_cwd,
          ["t"] = fb_actions.change_cwd,
          [" "] = fb_actions.toggle_browser,
          ["h"] = fb_actions.toggle_hidden,
          ["s"] = fb_actions.toggle_all,
        },
      },
    }
  }
}

require("telescope").load_extension "file_browser"
