------------------------------------------------------------------
--                        NVIM-AUTOPAIRS                        --
------------------------------------------------------------------
-- https://github.com/windwp/nvim-autopairs                     --
------------------------------------------------------------------

  require("nvim-autopairs").setup()

  local cmp_autopairs = require('nvim-autopairs.completion.cmp')
  local cmp = require('cmp')

  cmp.event:on(
    'confirm_done',
    cmp_autopairs.on_confirm_done()
  )

------------------------------------------------------------------
--                        NVIM-TS-AUTOTAG                       --
------------------------------------------------------------------
-- https://github.com/windwp/nvim-ts-autotag                    --
------------------------------------------------------------------

  require('nvim-ts-autotag').setup()

  vim.lsp.handlers['textDocument/publishDiagnostics'] = vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics,
    {
        underline = true,
        virtual_text = {
            spacing = 5,
            severity_limit = 'Warning',
        },
        update_in_insert = true,
    }
  )

------------------------------------------------------------------
--                           LSPKIND                            --
------------------------------------------------------------------
-- https://github.com/onsails/lspkind.nvim                      --
------------------------------------------------------------------

  local lspkind = require('lspkind')

  cmp.setup {
    formatting = {
      format = lspkind.cmp_format({
        mode = 'symbol_text',
        maxwidth = 50,
        ellipsis_char = '...',
      }),
    }
  }

    cmp.setup {
      mapping = {
        ['<S-Tab>'] = cmp.mapping(function(fallback)
          local copilot_keys = vim.fn['copilot#Accept']()
          if copilot_keys ~= '' and type(copilot_keys) == 'string' then
            vim.api.nvim_feedkeys(copilot_keys, 'i', true)
          else
            fallback()
          end
        end, {
          'i',
          's',
        }),
      },
      completion = {
        autocomplete = false,
      }
    }

vim.g.copilot_assume_mapped = true
vim.g.gitblame_message_when_not_committed = '早速コミットしろよ'
vim.g.indent_blankline_filetype_exclude = {
  'dashboard',
}
--vim.g.copilot_tab_fallback = ""
