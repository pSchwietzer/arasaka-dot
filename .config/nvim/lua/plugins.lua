------------------------------------------------------------------
--                            LAZY                              --
------------------------------------------------------------------
-- https://github.com/folke/lazy.nvim                           --
------------------------------------------------------------------

  --local neovim_version = vim.fn['serverversion']()['version']
  --local footer_text = {"ネオビム " .. neovim_version}

  return {
    checker = {
      notify = false,
    },
    {
      'nvim-lualine/lualine.nvim',
      dependencies = {
        'nvim-tree/nvim-web-devicons',
        opt = true
      },
    },
    'nvim-tree/nvim-web-devicons',
    --{
    --  'nvim-tree/nvim-tree.lua',
    --  dependencies = {
    --    'nvim-tree/nvim-web-devicons' -- optional, for file icons
    --  },
    --},
    {
      'akinsho/bufferline.nvim',
      dependencies = {
        'nvim-tree/nvim-web-devicons',
      },
    },
    {
      'akinsho/toggleterm.nvim',
      version = '*',
    },
    {
    'nvim-telescope/telescope.nvim', tag = '0.1.3',
      dependencies = {
        'nvim-lua/plenary.nvim'
      },
    },
    {
      "nvim-telescope/telescope-file-browser.nvim",
      dependencies = {
        "nvim-telescope/telescope.nvim",
        "nvim-lua/plenary.nvim"
      },
    },
    {
      'nvimdev/dashboard-nvim',
      event = 'VimEnter',
      config = function()
        require('dashboard').setup {
          theme = 'hyper',
          disable_move = false,
          shortcut_type = 'number',
          change_to_vcs_root = false,
          config = {
            project = {
              enable = false,
              limit = 8,
              icon = '　 ',
              label = '最近のプロジェクト',
              action = function(path)
                vim.cmd(':cd ' .. path)
                vim.cmd(':bdelete')
                vim.cmd('Telescope find_files disable_devicons=true')
              end,
            },
            mru = {
              limit = 8,
              icon = '　 ',
              label = '最近のファイル',
            },
            packages = {
              enable = false,
            },
            shortcut = {
              { desc = '新しいファイル', action = function() vim.cmd(':bdelete') end, key = 'e', group = '' },
              { desc = 'ファイルを開く', action = 'Telescope find_files disable_devicons=true', key = 'f', group = '' },
              { desc = 'フォルダーを開く', action = 'Telescope file_browser disable_devicons=true', key = 'd', group = '' },
              { desc = 'アップデート', action = 'Lazy sync', key = 'u', group = '' },
              { desc = 'ヘルプ', action = 'Telescope help_tags disable_devicons=true', key = 'h', group = '' },
              { desc = '終了', action = 'qa', key = 'q', group = '' },
            },
            header = {
              [[                         ......                         ]],
              [[                      .cx0XXXX0xc.                      ]],
              [[                     ;0WMMMMMMMMW0;                     ]],
              [[                    '0MMMMMMMMMMMM0'                    ]],
              [[              ..    ;XMMMMMMMMMMMMX;    ..              ]],
              [[          .cxO00Oxc.'kWMMMMMMMMMMWk'.cxO00Oxc.          ]],
              [[        .lXMMMMMMMWKl;dXWMMMMMMWXd;lKWMMMMMMWXl.        ]],
              [[        lNMMMMMMMMMMNl..cOWMMWOc..lNMMMMMMMMMMNl        ]],
              [[        kMMMMMMMMMMMMx.  cNMMNc  .xMMMMMMMMMMMMk        ]],
              [[        lNMMMMMMMMMMNl   cNMMNc   lNMMMMMMMMMMNl        ]],
              [[        .cKWMMMMMMMMNx'  cNMMNc  'xNMMMMMMMMWKc.        ]],
              [[          .cdk00kdxKWMXo'lNMMNl'oXMWKxdkO0kdc.          ]],
              [[                   .oKWMXXWMMWXXWWKo.                   ]],
              [[                     .oKWMMMMMMWKo.                     ]],
              [[                       .oXMMMMXo.                       ]],
              [[                         lNMMNl                         ]],
              [[                         cNMMNc                         ]],
              [[                         cNMMNc                         ]],
              [[                         .lool.                         ]],
              --[[                                                        ]]
              --[[ █████╗ ██████╗  █████╗ ███████╗ █████╗ ██╗  ██╗ █████╗ ]]
              --[[██╔══██╗██╔══██╗██╔══██╗██╔════╝██╔══██╗██║ ██╔╝██╔══██╗]]
              --[[███████║██████╔╝███████║███████╗███████║█████╔╝ ███████║]]
              --[[██╔══██║██╔══██╗██╔══██║╚════██║██╔══██║██╔═██╗ ██╔══██║]]
              --[[██║  ██║██║  ██║██║  ██║███████║██║  ██║██║  ██╗██║  ██║]]
              --[[╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝╚═╝  ╚═╝]]
              [[                                                        ]],
            },
            week_header = {
             enable = false,
            },
            footer = {
              [[]],
              [[ネオビム]],
            },
          },
          hide = {
            statusline = false,
            tabline = true,
            winbar = true,
          },
        }
      end,
      dependencies = { {'nvim-tree/nvim-web-devicons'} }
    },

    -- LSP SUPPORT
    'neovim/nvim-lspconfig',
    'williamboman/mason.nvim',
    'williamboman/mason-lspconfig.nvim',
    {
      'nvim-treesitter/nvim-treesitter',
      build = ':TSUpdate',
    },
    'onsails/lspkind.nvim',
    'arkav/lualine-lsp-progress',
    'LHKipp/nvim-nu',

    -- AUTOCOMPLETION
    'hrsh7th/nvim-cmp',
    'hrsh7th/cmp-buffer',
    'hrsh7th/cmp-path',
    'saadparwaiz1/cmp_luasnip',
    'hrsh7th/cmp-nvim-lsp',
    'hrsh7th/cmp-nvim-lua',
    'windwp/nvim-autopairs',
    'windwp/nvim-ts-autotag',
    'L3MON4D3/LuaSnip',
    'rafamadriz/friendly-snippets',

    'VonHeikemen/lsp-zero.nvim',
    'github/copilot.vim',

    -- GIT
    'nvim-lua/plenary.nvim',
    {
      'sindrets/diffview.nvim',
      dependencies = {
        'nvim-lua/plenary.nvim',
      },
    },
    'f-person/git-blame.nvim',

    -- QUALITY OF LIFE
    {
      'lukas-reineke/indent-blankline.nvim',
      commit = '9637670896b68805430e2f72cf5d16be5b97a22a',
    },
    'brenoprata10/nvim-highlight-colors',
    'famiu/bufdelete.nvim',
    {
      'tonyfettes/fcitx5.nvim',
      rocks = {
        'dbus_proxy',
        'lgi'
      },
    },
  }
