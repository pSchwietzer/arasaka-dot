------------------------------------------------------------------
--                            MASON                             --
------------------------------------------------------------------
-- https://github.com/williamboman/mason.nvim                   --
------------------------------------------------------------------

  require('mason').setup{
    ui = {
      border = "single",
      icons = {
        package_installed = "✓",
        package_pending = "➜",
        package_uninstalled = "✗"
      }
    }
  }

------------------------------------------------------------------
--                       NVIM-TREESITTER                        --
------------------------------------------------------------------
-- https://github.com/nvim-treesitter/nvim-treesitter           --
------------------------------------------------------------------

  require('nvim-treesitter.configs').setup{
    ensure_installed = 'all',
    highlight = {
      enable = true
    }
  }

------------------------------------------------------------------
--                          LSP-ZERO                            --
------------------------------------------------------------------
-- https://github.com/VonHeikemen/lsp-zero.nvim                 --
------------------------------------------------------------------

  --local lsp = require('lsp-zero')

  local lsp = require('lsp-zero').preset({
    name = 'recommended',
    configure_diagnostics = true,
    float_border = 'single',
  })

  lsp.ensure_installed({
    'tsserver',
    'eslint',
  })

  lsp.setup()

  local cmp = require('cmp')
  local cmp_config = lsp.defaults.cmp_config({
    window = {
      completion = cmp.config.window.bordered(),
      documentation = cmp.config.window.bordered(),
    }
  })

  cmp.setup(cmp_config)
